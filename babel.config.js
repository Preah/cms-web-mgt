module.exports = {
    presets: [
        '@vue/app',
        {
            presets: [
                ['@vue/app', {
                    polyfills: [
                        'es6.promise',
                        'es6.symbol'
                    ]
                }]
            ]
        }
    ]
}
// module.exports = {
//     presets: [
//         [
//             '@vue/app', {
//             useBuiltIns: 'entry'
//         }
//         ]
//     ]
// }
