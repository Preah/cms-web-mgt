# 프론트엔드 개발 예제(corpus-web-mgt)
  * frontend 개발을 위한 기본 개발환경을 제공한다.  


# 이력
  * v0.0.1          
    * 2019.09.01 
      * 최초 등록


# 구성

  <!-- blank line -->
  * 환경
    * windows 10, mac
    * visual studio code v1.36.1
    * sourcetree v2.7.6
    * node v10.16.0
    * npm v6.9.0
    * vue.js v3.9.2
    * typescript v3.5.3

  <!-- blank line -->
  * 라이브러리
    * vue-class-component
      * https://github.com/vuejs/vue-class-component
    * class-validator
      * https://github.com/typestack/class-validator
    * class-transformer
      * https://github.com/typestack/class-transformer
    * vuex-smart-module
      * https://github.com/ktsn/vuex-smart-module
    * vue-property-decorator
      * https://github.com/kaorun343/vue-property-decorator
    * axios
      * https://github.com/axios/axios
    * lodash
      * https://lodash.com
    * qs
      * https://github.com/ljharb/qs
     

# 설치 및 실행(<a target="_blank" rel="noopener noreferrer" href="http://133.186.171.48:9080/nikl/corpus-web-mgt/raw/develop/Guide/InstallGuide.webm">동영상</a>)
<!-- blank line -->
  * 설치
    * 1. Node 모듈 설치<a href="https://nodejs.org/ko/">Link</a>
    * 2. Git 설치<a href="https://git-scm.com/">Link</a>
    * 3. Webstrom 설치치<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>
  
  * 실행
    * npm install
    * npm run serve
    * npm run build

# UML
  ## UI 컴포넌트 구조
  <img src="http://133.186.171.48:9080/nikl/corpus-web-mgt/raw/develop/Guide/Vue-App.png" width="400" height="240" />
  
  ## Data 패턴
  <img src="http://133.186.171.48:9080/nikl/corpus-web-mgt/raw/develop/Guide/Vue-Data-Pattern.png" width="400" height="240" />
  
  ## UI 패턴
  <img src="http://133.186.171.48:9080/nikl/corpus-web-mgt/raw/develop/Guide/Vue-Page-Pattern.png" width="400" height="240" />
  
  
# 개발
  ## 패키지 구조   
  | 패키지 | 설명 |
  | --------                                   |  --------                |
  | /common             | 공통 패키지(비니지스 로직이 포함되지 않는 상위 영역 |
  | /sample             | 샘플 패키지(기본 CRUD)                              |
  | /corpus             | 비지니스 메인 영역                                  |
  | /corpus/common      | 비지니스 공통 영역                                  |
  | /corpus/login       | 비지니스 로그인 영역                                |
  | /corpus/mypage      | 비지니스 마이페이지 영역                            |
  | /corpus/mgt-corpus  | 비지니스 말뭉치 파일 관리 영역                      |
  | /corpus/mgt-project | 비지니스 프로젝트 영역                              |
  | /corpus/mgt-dept    | 비지니스 분과 영역                                  |
  | /corpus/build       | 비지니스 말뭉치 구축 영역                           |
  | /corpus/verify      | 비지니스 말뭉치 검증 영역                           |
  | /corpus/communicate | 비지니스 게시판 영역                                |
  | /corpus/setting     | 비지니스 분석 환경 설정 영역                        |
  | /corpus/system      | 비지니스 시스템 관리 영역                           |
  
  ## 기능별 패키지 구조   
  | 패키지 | 엔티티 | 설명 |
  | --------  |  --------  |  --------                                                                                 |
  | /corpus/login                                | Login	         | 로그인                                  | 
  | /corpus/mypage/download                      | Download	     | 다운로드                                | 
  | /corpus/mypage/note                          | Note	         | 쪽지                                    | 
  | /corpus/mypage/note                          | Block	         | 쪽지-차단                               | 
  | /corpus/mgt-corpus/corpus                    | Corpus	         | 말뭉치                                  | 
  | /corpus/mgt-corpus/paragraph                 | ParagraphEdit        | 문서-문단                               | 
  | /corpus/mgt-corpus/workset                   | Workset	         | 작업세트                                | 
  | /corpus/mgt-corpus/analyze/corpus            | TagCorpus    	 | 분석말뭉치                              | 
  | /corpus/mgt-corpus/analyze/tag/coreference   | Coreference	     | 분석말뭉치-태깅-상호참조해결            | 
  | /corpus/mgt-corpus/analyze/tag/coreference   | Mention	         | 분석말뭉치-태깅-상호참조해결-맨션       | 
  | /corpus/mgt-corpus/analyze/tag/zero          | Zero	         | 분석말뭉치-태깅-무형대용어복원          | 
  | /corpus/mgt-corpus/analyze/tag/zero          | Antecedent	     | 분석말뭉치-태깅-무형대용어복원-생략어   | 
  | /corpus/mgt-corpus/analyze/tag/morpheme      | Morpheme	     | 분석말뭉치-태깅-형태소                  | 
  | /corpus/mgt-corpus/analyze/tag/dependency    | Dependency	     | 분석말뭉치-태깅-구문                    | 
  | /corpus/mgt-corpus/analyze/tag/named         | Named	         | 분석말뭉치-태깅-개체명                  |  
  | /corpus/mgt-corpus/analyze/tag/sense         | Sense	         | 분석말뭉치-태깅-어휘의미                | 
  | /corpus/mgt-corpus/analyze/tag/semantic      | Semantic	     | 분석말뭉치-태깅-의미역                  | 
  | /corpus/mgt-corpus/analyze/tag/semantic      | Argument	     | 분석말뭉치-태깅-의미역-논항             | 
  | /corpus/mgt-project/project                  | Project	         | 프로젝트                                | 
  | /corpus/mgt-dept/dept                        | Dept	                | 분과-분과                               | 
  | /corpus/mgt-dept/dept                        | Worker	         | 분과-작업자                             | 
  | /corpus/mgt-dept/dept                        | Work	                | 분과-작업                               |
  | /corpus/communicate/board                    | Board	         | 게시판-게시판                           | 
  | /corpus/communicate/attach                   | Attach	         | 게시판-게시판첨부파일                   | 
  | /corpus/communicate/comment                  | Comment	         | 게시판-게시판댓글                       | 
  | /corpus/setting/tier                         | Tier	                | 분석층위                                | 
  | /corpus/setting/tag                          | TagCategory	        | 표지-표지유형                           | 
  | /corpus/setting/tag                          | Tag	                | 표지-표지                               | 
  | /corpus/system/account                       | Account	         | 사용자                                  |     
  | /corpus/system/role                          | Role	         | 역할                                    | 
  | /corpus/system/resource                      | Resource	     | 기능                                    | 
  | /corpus/system/menu                          | Menu	         | 메뉴                                    | 
  | /corpus/system/setup                         | Setup	         | 게시판-게시판설정                       | 
  | /corpus/system/code                          | Category	        | 코드-코드1                              |
  | /corpus/system/code                          | Type	                | 코드-코드2                              | 
  | /corpus/system/code                          | Constant	        | 코드-코드3                              |
  
 
  ## 기능별 공통 패키지 구조
  | 패키지 | 설명 |
  |  --------         |  --------            |
  | /sample/model     | 모델 영역            |
  | /sample/view      | 라우팅 컴포넌트 영역 |
  | /sample/component | 자식 컴포넌트 영역   |
  | /sample/router    | 라우팅 영역          |
  | /sample/state     | 상태 영역            |
  | /sample/action    | 제어 영역            |
  | /sample/mutation  | 상태 변경 영역       |
  | /sample/getter    | 상태 반환 영역       |
  | /sample/store     | 저장소 영역          |