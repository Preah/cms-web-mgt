import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
// import '@/assets/css/default.css';
import '@/assets/css/layout.css';
import '@/assets/css/nanumsquare.css';
import 'babel-polyfill';

import Vue              from 'vue';
import NicedayApp       from '@/NicedayApp.vue';
import router           from '@/router';
import BootstrapVue     from 'bootstrap-vue';
import Install          from '@/kr/co/genie/common/engine/plugin/Install';
import ClickConfirm     from '@/kr/co/genie/common/ui/components/ClickConfirm.vue';
import DatepickerFromTo from '@/kr/co/genie/common/ui/components/DatepickerFromTo.vue';
import DatepickerSingle from '@/kr/co/genie/common/ui/components/DatepickerSingle.vue';
import ValidateToasts   from '@/kr/co/genie/common/ui/components/ValidateToasts.vue';
import MessageToasts    from '@/kr/co/genie/common/ui/components/MessageToasts.vue';
import AxiosToasts      from '@/kr/co/genie/common/ui/components/AxiosToasts.vue';
import Spinner          from '@/kr/co/genie/common/ui/components/Spinner.vue';
import PageOption       from '@/kr/co/genie/common/ui/components/PageOption.vue';
import PageNavigation   from '@/kr/co/genie/common/ui/components/PageNavigation.vue';
import Select           from '@/kr/co/genie/common/ui/components/Select.vue';
import Radio            from '@/kr/co/genie/common/ui/components/Radio.vue';
import PageNumber       from '@/kr/co/genie/common/ui/components/PageNumber.vue';
import UserInfo         from '@/kr/co/genie/common/ui/components/UserInfo.vue';
import ModelSelect      from '@/kr/co/genie/common/ui/components/model-select/ModelSelect.vue';

Vue.config.devtools = process.env.NODE_ENV === 'local';
Vue.config.devtools = true;
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(Install);
Vue.component('nConfirm', ClickConfirm);
Vue.component('nDateFromTo', DatepickerFromTo);
Vue.component('nDateSingle', DatepickerSingle);
Vue.component('nValidateToasts', ValidateToasts);
Vue.component('nMessageToasts', MessageToasts);
Vue.component('nAxiosToasts', AxiosToasts);
Vue.component('nSpinner', Spinner);
Vue.component('nPageOption', PageOption);
Vue.component('nPageNavigation', PageNavigation);
Vue.component('nSelect', Select);
Vue.component('nRadio', Radio);
Vue.component('nPageNumber', PageNumber);
// Vue.component('nUserInfo', UserInfo);
Vue.component('nModelSelect', ModelSelect);

new Vue({
    router,
    render: (h) => h(NicedayApp),
}).$mount('#app');
