import Vue               from 'vue';
import Router            from 'vue-router';
import {RouteConfig}     from 'vue-router';

Vue.use(Router);​
const requireContext: __WebpackModuleApi.RequireContext = require.context('@/', true, /\w+Router.ts$/) ;
const routeConfigs  : Array<RouteConfig>                = new Array();
requireContext.keys().forEach(fileName => {
    [...requireContext(fileName).default].forEach(route => routeConfigs.push(route));
});


const routerMethods = ['push', 'replace'];

routerMethods.forEach((method: string) => {
    const originalCall = (Router.prototype as any)[method];
    (Router.prototype as any)[method] = function(location: any, onResolve: any, onReject: any): Promise<any> {
        if (onResolve || onReject) {
            return originalCall.call(this, location, onResolve, onReject);
        }
        return (originalCall.call(this, location) as any).catch((err: any) => err);
    };
});

const router = new Router({
     mode   : 'history'
    ,routes : routeConfigs
});

router.beforeEach((to, from, next) => {
    // to: 이동할 url에 해당하는 라우팅 객체
    if (to.matched.some(routeInfo => routeInfo.meta.requiresAuth)) {
        // 이동할 페이지에 인증 정보가 필요하면 경고 창을 띄우고 페이지 전환은 하지 않음
        const isLogin : string | null = localStorage.getItem('isLogin');
        if(isLogin === 'true') {
            next();
        } else {
            next({ path : '/login' });
        }
    } else {
        next(); // 페이지 전환
    }
});

export default router;