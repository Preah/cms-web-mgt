const DraggableRouter = [
    {
        path: '/draggable',
        component : () => import ('@/kr/co/genie/common/ui/layout/DefaultLayout.vue'),
        children  : [
            {
                path : 'sample',
                component: () => import('@/kr/co/genie/public/draggable/view/Draggable.vue'),
            },
            {
                path : 'sample-list',
                component: () => import('@/kr/co/genie/public/draggable/view/DraggableList.vue'),
            },
            {
                path : 'sample-list-file',
                component: () => import('@/kr/co/genie/public/draggable/view/DraggableListFile.vue'),
            },
            {
                path : 'sample2',
                component: () => import('@/kr/co/genie/public/draggable/view/SLTree.vue'),
            },
            {
                path : 'searchbar',
                component: () => import('@/kr/co/genie/public/draggable/view/SearchBar.vue'),
            }
        ]
    }
];

export default DraggableRouter;
