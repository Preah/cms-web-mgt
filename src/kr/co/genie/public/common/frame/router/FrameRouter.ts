const FrameRouter = [
    {
        path: '/common/frame',
        component : () => import ('@/kr/co/genie/common/ui/layout/DefaultLayout.vue'),
        children  : [
            {
                path : 'list',
                component: () => import('@/kr/co/genie/public/common/frame/view/FrameList.vue'),
            }
        ]
    }
];

export default FrameRouter;
