const FrameRouter = [
    {
        path: '/program',
        component : () => import ('@/kr/co/genie/common/ui/layout/DefaultLayout.vue'),
        children  : [
            {
                path : 'list',
                component: () => import('@/kr/co/genie/public/program/view/ProgramList.vue'),
            },
            {
                path : 'add',
                component: () => import('@/kr/co/genie/public/program/view/ProgramAdd.vue'),
            },
            {
                path : 'modify',
                component: () => import('@/kr/co/genie/public/program/view/ProgramModify.vue'),
            },
            {
                path : 'view',
                component: () => import('@/kr/co/genie/public/program/view/ProgramView.vue'),
            }
        ]
    }
];

export default FrameRouter;
