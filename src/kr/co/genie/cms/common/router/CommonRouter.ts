import Guide    from '@/kr/co/genie/common/ui/guide/Guide.vue';
import NotFound from '@/kr/co/genie/cms/common/view/NotFound.vue';

const CommonRouter = [
    {
        path: '/',
        component: Guide,
    },
    {
        path: '*',
        component: NotFound,
    },
];

export default CommonRouter;
