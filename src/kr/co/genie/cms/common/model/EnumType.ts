export namespace EnumType {

    // 프로젝트 타입
    export namespace Project {

        export enum Type {
            CNST = 'CNST',
            VERF = 'VERF',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.CNST, '구축'],
            [Type.VERF, '품질검증'],
        ]);

    }

    // 작업 타입
    export namespace Work {

        export enum Type {
            CNST   = 'CNST',
            VERF   = 'VERF',
            REVIEW = 'REVIEW',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.CNST,   '구축'],
            [Type.VERF,   '품질검증'],
            [Type.REVIEW, '검토'],
        ]);
    }

    // 분과 상태
    export namespace Dept {

        export enum Status {
            DIV_WAIT  = 'DIV_WAIT',
            WORK_WAIT = 'WORK_WAIT',
            WORK_ING  = 'WORK_ING',
            WORK_COMP = 'WORK_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.DIV_WAIT,  '분배대기'],
            [Status.WORK_WAIT, '작업대기'],
            [Status.WORK_ING,  '작업중'],
            [Status.WORK_COMP, '작업완료'],
        ]);
    }


    // 요청 방법
    export namespace Method {

        export enum Type {
            GET    = 'GET',
            POST   = 'POST',
            PUT    = 'PUT',
            DELETE = 'DELETE',
            HEAD   = 'HEAD',
            PATCH  = 'PATCH',
            NONE   = 'NONE',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.GET,    '조회'],
            [Type.POST,   '등록'],
            [Type.PUT,    '수정'],
            [Type.DELETE, '삭제'],
            [Type.HEAD,   '여부'],
            [Type.PATCH,  '일부수정'],
            [Type.NONE,   '없음'],
        ]);
    }

    // 허용 타입
    export namespace Permission {

        export enum Type {
            PERMIT = 'PERMIT',
            DENY   = 'DENY',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.PERMIT,  '모두허용'],
            [Type.DENY,    '권한허용'],
        ]);
    }

    // 범위
    export namespace Scope {

        export enum Type {
            ALL     = 'ALL',
            METHOD  = 'METHOD',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.ALL,    '전체메소드'],
            [Type.METHOD, '특정메소드'],
        ]);
    }

    // 계정 상태
    export namespace Account {

        export enum Status {
            WAIT    = 'WAIT',
            MGR     = 'MGR',
            ACTIVE  = 'ACTIVE',
            STOP    = 'STOP',
            LEAVE   = 'LEAVE',
            ROCK    = 'ROCK',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.WAIT,   '가입대기'],
            [Status.MGR,    '승인대기'],
            [Status.ACTIVE, '활성'],
            [Status.STOP,   '일시정지'],
            [Status.LEAVE,  '탈퇴'],
            [Status.ROCK,   '잠김'],
        ]);
    }

    // 권한
    export namespace Role {

        export enum Type {
            ROLE_SYSTEM  = 'ROLE_SYSTEM',
            ROLE_CORPUS  = 'ROLE_CORPUS',
            ROLE_PROJECT = 'ROLE_PROJECT',
            ROLE_DEPT    = 'ROLE_DEPT',
            ROLE_WORK    = 'ROLE_WORK',
            ROLE_USER    = 'ROLE_USER',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.ROLE_SYSTEM,  '시스템관리자'],
            [Type.ROLE_CORPUS,  '말뭉치관리자'],
            [Type.ROLE_PROJECT, '프로젝트관리자'],
            [Type.ROLE_DEPT,    '분과관리자'],
            [Type.ROLE_WORK,    '자료작업자'],
            [Type.ROLE_USER,    '사용자'],
        ]);
    }

    // 게시판
    export namespace Setup {

        export enum Type {
            NOTICE = 'NOTICE',
            DATA   = 'DATA',
            DEPT   = 'DEPT',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.NOTICE, '공지사항'],
            [Type.DATA,   '자료실'],
            [Type.DEPT,   '분과'],
        ]);
    }

    // 다운로드
    export namespace Download {

        // 상태
        export enum Status {
            WAIT   = 'WAIT',
            START  = 'START',
            END    = 'END',
            CANCEL = 'CANCEL',
            FAIL   = 'FAIL',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.WAIT,   '대기중'],
            [Status.START,  '시작'],
            [Status.END,    '종료'],
            [Status.CANCEL, '취소'],
            [Status.FAIL,   '실패'],
        ]);

         // 타입
        export enum Type {
            RAW       = 'RAW',
            WORKSET   = 'WORKSET',
            WORK_STAT = 'WORK_STAT',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.RAW,      '원시말뭉치'],
            [Type.WORKSET,  '작업세트'],
            [Type.WORK_STAT, '작업통계'],
        ]);
    }


    // 파일 타입
    export namespace File {

        export enum Type {
            JSON  = 'JSON',
            XML   = 'XML',
            CONLL = 'CONLL',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.JSON, 'JSON'],
            [Type.XML,  'XML'],
            // [Type.CONLL,'CoNLL'],
        ]);
    }

    // 말뭉치 타입
    export namespace Corpus {

        export namespace Group {

            export enum Type {
                W = 'W',
                S = 'S',
                N = 'N',
                E = 'E',
                M = 'M',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.W, '문어 말뭉치'],
                [Type.S, '구어 말뭉치'],
                [Type.N, '신문 말뭉치'],
                [Type.E, '웹 말뭉치'],
                [Type.M, '메신저 말뭉치'],
            ]);
        }

        export namespace  W {

            export enum Type {
                WA = 'WA',
                WB = 'WB',
                WC = 'WC',
                WZ = 'WZ',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.WA, '책-상상'],
                [Type.WB, '책-정보'],
                [Type.WC, '잡지'],
                [Type.WZ, '기타'],
            ]);
        }

        export namespace S {

            export enum Type {
                SA = 'SA',
                SB = 'SB',
                SC = 'SC',
                SD = 'SD',
                SE = 'SE',
                SF = 'SF',
                SZ = 'SZ',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.SA, '공적 독백'],
                [Type.SB, '공적 대화'],
                [Type.SC, '사적 독백'],
                [Type.SD, '사적 대화'],
                [Type.SE, '준구어-대본'],
                [Type.SF, '준구어-연설'],
                [Type.SZ, '기타'],
            ]);
        }


        export namespace N {

            export enum Type {
                NW = 'NW',
                NL = 'NL',
                NP = 'NP',
                NI = 'NI',
                NZ = 'NZ',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.NW, '전국 종합지'],
                [Type.NL, '지역 종합지'],
                [Type.NP, '전문지'],
                [Type.NI, '인터넷 기반 신문'],
                [Type.NZ, '기타'],
            ]);
        }

        export namespace E {

            export enum Type {
                ES = 'ES',
                EB = 'EB',
                EP = 'EP',
                ER = 'ER',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.ES, '누리소통망'],
                [Type.EB, '블로그'],
                [Type.EP, '게시글'],
                [Type.ER, '리뷰'],
            ]);
        }

        export namespace M {

            export enum Type {
                MD = 'MD',
                MM = 'MM',
            }

            export const TypeName: Map<Type | string, string> = new Map([
                [Type.MD, '2인 대화'],
                [Type.MM, '다자 대화'],
            ]);
        }
    }

    // 문서 상태
    export namespace Document {

        export enum Status {
            EDIT_WAIT = 'EDIT_WAIT',
            EDIT_ING  = 'EDIT_ING',
            EDIT_COMP = 'EDIT_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.EDIT_WAIT, '편집대기'],
            [Status.EDIT_ING,  '편집중'],
            [Status.EDIT_COMP, '편집완료'],
        ]);
    }

    // 편집 상태
    export namespace ParagraphEdit {

        export enum Status {
            EDIT_WAIT = 'EDIT_WAIT',
            EDIT_ING  = 'EDIT_ING',
            EDIT_COMP = 'EDIT_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.EDIT_WAIT, '편집대기'],
            [Status.EDIT_ING,  '편집중'],
            [Status.EDIT_COMP, '편집완료'],
        ]);
    }

    // 분석 말뭉치 상태
    export namespace AnalDocument {

        export enum Status {
            DIV_WAIT    = 'DIV_WAIT',
            WORK_WAIT   = 'WORK_WAIT',
            WORK_ING    = 'WORK_ING',
            VERF_WAIT   = 'VERF_WAIT',
            UPD_WAIT    = 'UPD_WAIT',
            VERF_COMP   = 'VERF_COMP',
            DCSN        = 'DCSN',
            DCSN_CANCEL = 'DCSN_CANCEL',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.DIV_WAIT,    '분배대기'],
            [Status.WORK_WAIT,   '작업대기'],
            [Status.WORK_ING,    '작업중'],
            [Status.VERF_WAIT,   '검토대기'],
            [Status.UPD_WAIT,    '수정대기'],
            [Status.VERF_COMP,   '검토완료'],
            [Status.DCSN,        '확정'],
            [Status.DCSN_CANCEL, '확정취소'],
        ]);
    }

    // 작업
    export namespace Workset {

        // 상태
        export enum Status {
            EXTC_ING   = 'EXTC_ING',
            EXTC_COMP  = 'EXTC_COMP',
            EDIT_ING   = 'EDIT_ING',
            EDIT_COMP  = 'EDIT_COMP',
            REG_COMP   = 'REG_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.EXTC_ING,  '추출중'],
            [Status.EXTC_COMP, '추출완료'],
            [Status.EDIT_ING,  '편집중'],
            [Status.EDIT_COMP, '편집완료'],
            [Status.REG_COMP,  '등록완료'],
        ]);

        // 타입
        export enum Type {
            CNST = 'CNST',
            VERF = 'VERF',
        }

        export const TypeName: Map<Type | string, string> = new Map([
            [Type.CNST, '구축'],
            [Type.VERF, '품질검증'],
        ]);
    }


    // 분석작업세트
    export namespace AnalWorkset {

        // 상태
        export enum Status {
            AUTO_ANALS_NON    = 'AUTO_ANALS_NON',
            AUTO_ANALS_ING    = 'AUTO_ANALS_ING',
            AUTO_ANALS_COMP   = 'AUTO_ANALS_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            // [Status.AUTO_ANALS_NON,  '자동분석없음'],
            [Status.AUTO_ANALS_NON,  '자동분석'],
            [Status.AUTO_ANALS_ING,  '자동분석중'],
            [Status.AUTO_ANALS_COMP, '자동분석완료'],
        ]);
    }


    // 말뭉치품질검증
    export namespace VerifyDept {

        export enum Status {
            QV_ING  = 'QV_ING',
            QV_COMP = 'QV_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.QV_ING,  '품질검증중'],
            [Status.QV_COMP,  '품질검증완료'],
        ]);
    }


    // 자동학습
    export namespace Learn {

        export enum Status {
            LEARN_ING  = 'LEARN_ING',
            LEARN_COMP = 'LEARN_COMP',
        }

        export const StatusName: Map<Status | string, string> = new Map([
            [Status.LEARN_ING,  '학습 중'],
            [Status.LEARN_COMP, '학습 완료'],
        ]);
    }
}
