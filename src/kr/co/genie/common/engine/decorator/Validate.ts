import {ValidateToastsEvent} from '@/kr/co/genie/common/engine/event/ValidateToastsEvent';

export function Validate(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const original = descriptor.value;
    descriptor.value = function(...args: any) {
        return Object.$plugins.validator.validate(args[0]).then((errors) => {

            if (Object.$plugins.lodash.isEmpty(errors)) {
                return original.apply(this, args);
            }

            ValidateToastsEvent.$emit('validate-toasts', errors);
            return false;
        });
    };
}
