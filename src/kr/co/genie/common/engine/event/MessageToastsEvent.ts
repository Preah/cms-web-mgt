import Vue from 'vue';

export const MessageToastsEvent = new Vue();

export class MessageToast {

    public onMessageToasts = (messageClass: Message): void => {
        MessageToastsEvent.$emit('message-toasts', messageClass);
    }
}

export class Message {
    public variant: Variant = Variant.primary;
    public title: string = '';
    public contents: string = '';

    constructor(variant: Variant, title: string, contents: string) {
        this.variant = variant;
        this.title = title === '' ? ' ' : title;
        this.contents = contents === '' ? ' ' : contents;
    }
}

export enum Variant {
    primary = 'primary',
    secondary = 'secondary',
    success = 'success',
    warning = 'warning',
    danger = 'danger',
    info = 'info',
    light = 'light',
    dark = 'dark',
}
