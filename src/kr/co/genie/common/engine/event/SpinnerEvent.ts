import Vue from 'vue';

export const SpinnerEvent = new Vue();

export class Spinner {
    public onSpinnerAxios = (useYn: boolean): void => {
        SpinnerEvent.$emit('on-spinner-axios', useYn);
    }
}
