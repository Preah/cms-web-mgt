import Vue from 'vue';

export const AxiosToastsEvent = new Vue();


export class AxiosToast {

    public onAxiosErrorToasts = (error: any): void => {
        AxiosToastsEvent.$emit('axios-error-toasts', error);
    }
}
