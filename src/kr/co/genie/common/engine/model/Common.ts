import 'reflect-metadata';
import {Expose}      from 'class-transformer';
import {Description} from '@/kr/co/genie/common/engine/decorator/Description';

export namespace Common {
    export namespace Request {

        export class Search {

            @Expose() @Description('조건')
            public condition!: string;

            @Expose() @Description('키워드')
            public keyword!: null;
        }

        export class Page {

            @Expose() @Description('페이지번호')
            public page: number = 0;

            @Expose() @Description('페이지크기')
            public size!: number;

            @Expose() @Description('검색')
            public base: Search = new Search;
        }
    }

    export namespace Response {

        export class Account {

            @Expose() @Description('사용자일련번호')
            public id !: number;

            @Expose() @Description('이름')
            public name !: string;

            @Expose() @Description('아이디')
            public identifier !: string;
        }

        export class Page {

            @Expose() @Description('페이지크기')
            public size !: number;

            @Expose() @Description('페이지번호')
            public number !: number;

            @Expose() @Description('전체페이지수')
            public totalPages !: number;

            @Expose() @Description('전체게시물수')
            public totalElements !: number;

            @Expose() @Description('페이지게시물수')
            public numberOfElements !: number;

            @Expose() @Description('첫페이지여부')
            public first !: boolean;

            @Expose() @Description('마지막페이지여부')
            public last !: boolean;

            @Expose() @Description('목록')
            public content !: Array<any>;
        }
    }
}
