import AxiosHttp from 'axios';
import qs        from 'qs';

const axiosBasic = AxiosHttp.create({
    baseURL: process.env.VUE_APP_AUTH_API_URL !== undefined ? process.env.VUE_APP_AUTH_API_URL : 'http://corpus.diquest.com:8926',
    paramsSerializer: (params) => qs.stringify(params, {encoding: false, allowDots: true}),
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': process.env.VUE_APP_BASIC_TOKEN,
    },
});

axiosBasic.interceptors.request.use(
    (conf) => {
        Object.$plugins.event.onSpinnerAxios(true);
        return conf;
    },
    (error) => {
        Object.$plugins.event.onSpinnerAxios(false);
        return Promise.reject(error);
    },
);

axiosBasic.interceptors.response.use(
    (response) => {
        Object.$plugins.event.onSpinnerAxios(false);
        return response;
    },
    (error) => {
        Object.$plugins.event.onSpinnerAxios(false);
        return Promise.reject(error);
    },
);

export default axiosBasic;
