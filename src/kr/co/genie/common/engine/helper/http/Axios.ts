import AxiosHttp      from 'axios';
import qs             from 'qs';

const axios = AxiosHttp.create({
    baseURL: process.env.VUE_APP_API_URL !== undefined ? process.env.VUE_APP_API_URL : 'http://corpus.diquest.com:8926',
    paramsSerializer: (params) => qs.stringify(params, {encoding: false, allowDots: true}),
    timeout: 1000 * 60,
});

axios.interceptors.request.use(
    conf => {
        const accessToken = localStorage.getItem('access_token');
        if (accessToken) {
            const key = 'Authorization';
            conf.headers[key] = `Bearer ${accessToken}`;
        }
        return conf;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.request.use(
    (conf) => {
        Object.$plugins.event.onSpinnerAxios(true);
        return conf;
    },
    (error) => {
        Object.$plugins.event.onSpinnerAxios(false);
        return Promise.reject(error);
    },
);

axios.interceptors.response.use(
    (response) => {
        Object.$plugins.event.onSpinnerAxios(false);
        return response;
    },
    (error) => {
        Object.$plugins.event.onSpinnerAxios(false);
        Object.$plugins.event.onAxiosError(error);
        return Promise.reject(error);
    },
);

export default axios;
