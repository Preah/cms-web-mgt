import * as lodash    from 'lodash';
import * as validator from 'class-validator';
import {Mapper}       from '@/kr/co/genie/common/engine/plugin/custom/Mapper';
import {Plugin}       from '@/kr/co/genie/common/engine/plugin/Plugin';
import {Event}        from '@/kr/co/genie/common/engine/plugin/custom/Event';
import {Enum}         from '@/kr/co/genie/common/engine/plugin/custom/Enum';
import moment         from 'moment-timezone';
import numeral        from 'numeral';

export default {

   install() {
       Object.defineProperty(Object, '$plugins', {
           get : (): Plugin => {
               return new Plugin (
                    new Mapper
                   ,new validator.Validator
                   ,Object.create(lodash)
                   ,new Event
                   ,moment
                   ,numeral
               );
           },
       });

       Object.defineProperty(Object, '$enum', {
           get : (): Enum => {
               return new Enum;
           },
       });
   },
};
