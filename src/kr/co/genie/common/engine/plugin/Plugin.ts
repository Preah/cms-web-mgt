import * as validator from 'class-validator';
import * as lodash    from 'lodash';
import {Mapper}       from '@/kr/co/genie/common/engine/plugin/custom/Mapper';
import {Event}        from '@/kr/co/genie/common/engine/plugin/custom/Event';
import {Enum}         from '@/kr/co/genie/common/engine/plugin/custom/Enum';
import moment         from 'moment-timezone';
import numeral        from 'numeral';

declare global {
    interface Object {
        $plugins: IPlugin;
        $enum: Enum;
    }

    interface IPlugin {
        mapper: Mapper<any>;
        validator: validator.Validator;
        lodash: lodash.LoDashStatic;
        event: Event;
        moment: moment;
        numeral: numeral;
    }
}

export class Plugin implements IPlugin {

    public lodash!: lodash.LoDashStatic;
    public mapper!: Mapper<any>;
    public validator!: validator.Validator;
    public event!: Event;
    public moment!: moment;
    public numeral!: numeral;

    // tslint:disable-next-line:no-shadowed-variable
    constructor(mapper: Mapper<any>, validator: validator.Validator, lodash: lodash.LoDashStatic, event: Event, moment: moment, numeral: numeral) {
        this.mapper = mapper;
        this.validator = validator;
        this.lodash = lodash;
        this.event = event;
        this.moment = moment;
        this.numeral = numeral;
    }
}
