import {plainToClass}     from 'class-transformer';
import {ClassTransformer} from 'class-transformer';
import {ClassType}        from 'class-transformer/ClassTransformer';
import {Common}           from '@/kr/co/genie/common/engine/model/Common';

export class Mapper<T> extends ClassTransformer {

    public toObject = (type: ClassType<T>, source: T): T => {
        return plainToClass(type, source, {excludeExtraneousValues: true});
    }

    public toArray = (type: ClassType<T>, source: Array<T>): Array<T> => {
        return plainToClass(type, source, {excludeExtraneousValues: true});
    }

    public toPage = (type: ClassType<T>, source: Common.Response.Page): Common.Response.Page => {
        const page = plainToClass(Common.Response.Page, source, {excludeExtraneousValues: true});
        page.content = plainToClass(type, source.content, {excludeExtraneousValues: true});
        return page;
    }
}
