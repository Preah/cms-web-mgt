import {Message, MessageToast, Variant} from '@/kr/co/genie/common/engine/event/MessageToastsEvent.ts';
import {Spinner}                        from '@/kr/co/genie/common/engine/event/SpinnerEvent.ts';
import {AxiosToast}                     from '@/kr/co/genie/common/engine/event/AxiosToastsEvent';

export class Event {
    public onMessage = (contents: string): void => {
        new MessageToast()
            .onMessageToasts(new Message(Variant.warning, '오류', contents));
    }

    public onMessageCustom = (variant: string, title: string, contents: string): void => {
        new MessageToast()
            .onMessageToasts(new Message(Variant[variant], title, contents));
    }

    public onSpinnerAxios = (useYn: boolean): void => {
        new Spinner().onSpinnerAxios(useYn);
    }

    public onAxiosError = (error: any): void => {
        new AxiosToast().onAxiosErrorToasts(error);
    }

}
