import {EnumType} from '@/kr/co/genie/cms/common/model/EnumType';

export class Enum {
//
//     public options = (typeName: any): Array<object> => {
//         const option: Array<{ text, value }> = new Array<{ text, value }>();
//         typeName.forEach((v, k) => {
//             option.push({text: v, value: k});
//         });
//         return option;
//     }
//
//     get ProjectType()              { return EnumType.Project.Type;        }
//     get WorkType()                 { return EnumType.Work.Type;           }
//     get DeptStatus()               { return EnumType.Dept.Status;         }
//     get MethodType()               { return EnumType.Method.Type;         }
//     get PermissionType()           { return EnumType.Permission.Type;     }
//     get ScopeType()                { return EnumType.Scope.Type;          }
//     get AccountStatus()            { return EnumType.Account.Status;      }
//     get RoleType()                 { return EnumType.Role.Type;           }
//     get SetupType()                { return EnumType.Setup.Type;          }
//     get DownloadType()             { return EnumType.Download.Type;       }
//     get DownloadStatus()           { return EnumType.Download.Status;     }
//     get FileType()                 { return EnumType.File.Type;           }
//     get DocumentStatus()           { return EnumType.Document.Status;     }
//     get ParagraphEditStatus()      { return EnumType.ParagraphEdit.Status;}
//     get AnalDocumentStatus()       { return EnumType.AnalDocument.Status; }
//     get WorksetStatus()            { return EnumType.Workset.Status;      }
//     get WorksetType()              { return EnumType.Workset.Type;        }
//     get CorpusWType()              { return EnumType.Corpus.W.Type;       }
//     get CorpusSType()              { return EnumType.Corpus.S.Type;       }
//     get CorpusNType()              { return EnumType.Corpus.N.Type;       }
//     get CorpusEType()              { return EnumType.Corpus.E.Type;       }
//     get CorpusMType()              { return EnumType.Corpus.M.Type;       }
//     get CorpusGroupType()          { return EnumType.Corpus.Group.Type;   }
//     get AnalWorksetStatus()        { return EnumType.AnalWorkset.Status;  }
//     get LearnStatus()              { return EnumType.Learn.Status;        }
//
//     get getProjectTypes()          { return this.options(EnumType.Project.TypeName);         }
//     get getWorkTypes()             { return this.options(EnumType.Work.TypeName);            }
//     get getDeptStatuses()          { return this.options(EnumType.Dept.StatusName);          }
//     get getMethodTypes()           { return this.options(EnumType.Method.TypeName);          }
//     get getPermissionTypes()       { return this.options(EnumType.Permission.TypeName);      }
//     get getScopeTypes()            { return this.options(EnumType.Scope.TypeName);           }
//     get getAccountStatuses()       { return this.options(EnumType.Account.StatusName);       }
//     get getRoleTypes()             { return this.options(EnumType.Role.TypeName);            }
//     get getSetupTypes()            { return this.options(EnumType.Setup.TypeName);           }
//     get getDownloadTypes()         { return this.options(EnumType.Download.TypeName);        }
//     get getDownloadStatuses()      { return this.options(EnumType.Download.StatusName);      }
//     get getFileTypes()             { return this.options(EnumType.File.TypeName);            }
//     get getDocumentStatuses()      { return this.options(EnumType.Document.StatusName);      }
//     get getParagraphEditStatuses() { return this.options(EnumType.ParagraphEdit.StatusName); }
//     get getAnalDocumentStatuses()  { return this.options(EnumType.AnalDocument.StatusName);  }
//     get getWorksetStatuses()       { return this.options(EnumType.Workset.StatusName);       }
//     get getWorksetTypes()          { return this.options(EnumType.Workset.TypeName);         }
//     get getCorpusWTypes()          { return this.options(EnumType.Corpus.W.TypeName);        }
//     get getCorpusSTypes()          { return this.options(EnumType.Corpus.S.TypeName);        }
//     get getCorpusNTypes()          { return this.options(EnumType.Corpus.N.TypeName);        }
//     get getCorpusETypes()          { return this.options(EnumType.Corpus.E.TypeName);        }
//     get getCorpusMTypes()          { return this.options(EnumType.Corpus.M.TypeName);        }
//     get getCorpusTypes()           {
//         return this.options(new Map([...EnumType.Corpus.W.TypeName,
//                                                 ...EnumType.Corpus.S.TypeName,
//                                                 ...EnumType.Corpus.N.TypeName,
//                                                 ...EnumType.Corpus.E.TypeName,
//                                                 ...EnumType.Corpus.M.TypeName,
//     ]));
//     }
//     get getCorpusGroupTypes()      { return this.options(EnumType.Corpus.Group.TypeName);    }
//     get getAnalWorksetStatuses()   { return this.options(EnumType.AnalWorkset.StatusName);   }
//     get getVerifyDeptStatuses()    { return this.options(EnumType.VerifyDept.StatusName);    }
//     get getLearnStatuses()         { return this.options(EnumType.Learn.StatusName);         }
//
//
//     public getProjectTypeName(key: any)         { return EnumType.Project.TypeName.get(key) as string;         }
//     public getWorkTypeName(key: any)            { return EnumType.Work.TypeName.get(key) as string;            }
//     public getDeptStatusName(key: any)          { return EnumType.Dept.StatusName.get(key) as string;          }
//     public getMethodTypeName(key: any)          { return EnumType.Method.TypeName.get(key) as string;          }
//     public getPermissionTypeName(key: any)      { return EnumType.Permission.TypeName.get(key) as string;      }
//     public getScopeTypeName(key: any)           { return EnumType.Scope.TypeName.get(key) as string;           }
//     public getAccountStatusName(key: any)       { return EnumType.Account.StatusName.get(key) as string;       }
//     public getRoleTypeName(key: any)            { return EnumType.Role.TypeName.get(key) as string;            }
//     public getSetupTypeName(key: any)           { return EnumType.Setup.TypeName.get(key) as string;           }
//     public getDownloadTypeName(key: any)        { return EnumType.Download.TypeName.get(key) as string;        }
//     public getDownloadStatusName(key: any)      { return EnumType.Download.StatusName.get(key) as string;      }
//     public getFileTypeName(key: any)            { return EnumType.File.TypeName.get(key) as string;            }
//     public getDocumentStatusName(key: any)      { return EnumType.Document.StatusName.get(key) as string;      }
//     public getParagraphEditStatusName(key: any) { return EnumType.ParagraphEdit.StatusName.get(key) as string; }
//     public getAnalDocumentStatusName(key: any)  { return EnumType.AnalDocument.StatusName.get(key) as string;  }
//     public getWorksetStatusName(key: any)       { return EnumType.Workset.StatusName.get(key) as string;       }
//     public getWorksetTypeName(key: any)         { return EnumType.Workset.TypeName.get(key) as string;         }
//     public getCorpusTypeName(key: any)          {
//         return new Map([...EnumType.Corpus.W.TypeName,
//                                   ...EnumType.Corpus.S.TypeName,
//                                   ...EnumType.Corpus.N.TypeName,
//                                   ...EnumType.Corpus.E.TypeName,
//                                   ...EnumType.Corpus.M.TypeName,
//         ]).get(key) as string;
//     }
//     public getCorpusGroupTypeName(key: any)     { return EnumType.Corpus.Group.TypeName.get(key) as string;    }
//     public getAnalWorksetStatusName(key: any)   { return EnumType.AnalWorkset.StatusName.get(key) as string;   }
//     public getVerifyDeptStatuseName(key: any)   { return EnumType.VerifyDept.StatusName.get(key) as string;    }
//     public getLearnStatuseName(key: any)        { return EnumType.Learn.StatusName.get(key) as string;         }
}
