module.exports = {
    configureWebpack: (config) => {
        config.entry = ["babel-polyfill", "./src/main.ts"]
    },
}
